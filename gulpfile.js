//88b           d88                        88               88                         
//888b         d888                        88               88                         
//88`8b       d8'88                        88               88                         
//88 `8b     d8' 88   ,adPPYba,    ,adPPYb,88  88       88  88   ,adPPYba,  ,adPPYba,  
//88  `8b   d8'  88  a8"     "8a  a8"    `Y88  88       88  88  a8P_____88  I8[    ""  
//88   `8b d8'   88  8b       d8  8b       88  88       88  88  8PP"""""""   `"Y8ba,   
//88    `888'    88  "8a,   ,a8"  "8a,   ,d88  "8a,   ,a88  88  "8b,   ,aa  aa    ]8I  
//88     `8'     88   `"YbbdP"'    `"8bbdP"Y8   `"YbbdP'Y8  88   `"Ybbd8"'  `"YbbdP"'  

//Node-----------------------------------------
var fs = require('fs');
var path = require('path');
var del = require('del');
//Gulp-----------------------------------------
var gulp = require('gulp');
//General Use----------------------------------
var rename = require('gulp-rename');
var gulpif = require('gulp-if');
//HTMl-----------------------------------------
var htmlmin = require('gulp-htmlmin');
//Style/Script---------------------------------
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var cssnano = require('gulp-cssnano');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');
var ngAnnotate = require('gulp-ng-annotate');
//Server---------------------------------------
var bs = require('browser-sync').create();


//8b           d8                        88              88           88                         
//`8b         d8'                        ""              88           88                         
// `8b       d8'                                         88           88                         
//  `8b     d8'  ,adPPYYba,  8b,dPPYba,  88  ,adPPYYba,  88,dPPYba,   88   ,adPPYba,  ,adPPYba,  
//   `8b   d8'   ""     `Y8  88P'   "Y8  88  ""     `Y8  88P'    "8a  88  a8P_____88  I8[    ""  
//    `8b d8'    ,adPPPPP88  88          88  ,adPPPPP88  88       d8  88  8PP"""""""   `"Y8ba,   
//     `888'     88,    ,88  88          88  88,    ,88  88b,   ,a8"  88  "8b,   ,aa  aa    ]8I  
//      `8'      `"8bbdP"Y8  88          88  `"8bbdP"Y8  8Y"Ybbd8"'   88   `"Ybbd8"'  `"YbbdP"'  

var addSourcemaps = false;

var paths = {
    src: {
        src: 'src',
        scripts: 'assets/scripts',
        styles: 'assets/styles',
		images: 'assets/images',
		files: 'assets/files'
    },
    dist: {
        styles: 'build/assets/styles',
        scripts: 'build/assets/scripts',
		images: 'build/assets/images',
		files: 'build/assets/files',
        build: 'build'
    }
};

var dependencyScripts = [
    paths.src.scripts + '/angular.min.js',
    paths.src.scripts + '/angular-animate.min.js',
    paths.src.scripts + '/angular-aria.min.js',
    paths.src.scripts + '/angular-messages.min.js',
    paths.src.scripts + '/angular-material.min.js',
    paths.src.scripts + '/isoCurrency.min.js',
    paths.src.scripts + '/moment.min.js'
];

//Server settings (BrowserSync)
//http://www.browsersync.io/docs/options/
var serverSettings = {
	host: 'localhost',
	port: 8888,
	ui: {
		port: 9999,
		weinre: {
			port: 8899
		}
	},
	server: {
		baseDir: paths.dist.build
	},
	ghostMode: true,
	online: false,
	watchOptions: {
		debounceDelay: 500
	}
};

var autoprefixSettings = {
	browsers: ['> 0%', 'ie >= 7'],
	remove: false //Remove existing prefixes
};

var minifyHTMLOptions = {
	removeComments: true,
	collapseWhitespace: true,
	removeAttributeQuotes: true
};

var cssnanoSettings = {
	discardUnused: {
		fontFace: false,
		keyframes: false
	}
};


// 888888888888                    88                    
//      88                         88                    
//      88                         88                    
//      88  ,adPPYYba,  ,adPPYba,  88   ,d8   ,adPPYba,  
//      88  ""     `Y8  I8[    ""  88 ,a8"    I8[    ""  
//      88  ,adPPPPP88   `"Y8ba,   8888[       `"Y8ba,   
//      88  88,    ,88  aa    ]8I  88`"Yba,   aa    ]8I  
//      88  `"8bbdP"Y8  `"YbbdP"'  88   `Y8a  `"YbbdP"'  

gulp.task('clean', function (done) {
	return del(paths.dist.build, done);
});
gulp.task('server', function(done) {
	bs.init(serverSettings, done);
});
gulp.task('reload', function (done) {
	bs.reload();
	done();
});
gulp.task('sass', function() {
    return gulp.src(paths.src.styles+'/**/*.scss')
	.pipe(gulpif(addSourcemaps, sourcemaps.init()))
    .pipe(sass())
	.pipe(rename({suffix: '.min'}))
	.pipe(cssnano(cssnanoSettings))
	.pipe(autoprefixer(autoprefixSettings))
	.pipe(gulpif(addSourcemaps, sourcemaps.write('./')))
	.pipe(gulp.dest(paths.dist.styles));
});
gulp.task('css:min', function() {
    return gulp.src(paths.src.styles+'/**/*.min.css')
	.pipe(gulp.dest(paths.dist.styles));
});
gulp.task('html', function() {
    return gulp.src(paths.src.src+'/**/*.html')
      .pipe(htmlmin(minifyHTMLOptions))
      .pipe(gulp.dest(paths.dist.build));
});
gulp.task('scripts', function() {
    return gulp.src(paths.src.scripts+'/**/*.min.js')
      .pipe(gulp.dest(paths.dist.scripts));
});
gulp.task('scripts:concat:dependencies', function() {
    return gulp.src(dependencyScripts)
      .pipe(gulpif(addSourcemaps, sourcemaps.init()))
      .pipe(concat("dependencies.min.js"))
      .pipe(uglify({ mangle: false }))
	  .pipe(gulpif(addSourcemaps, sourcemaps.write({includeContent: true})))
      .pipe(gulp.dest(paths.dist.scripts));
});
gulp.task('scripts:concat', function() {
    return gulp.src(paths.src.src + '/**/*.js')
      .pipe(gulpif(addSourcemaps, sourcemaps.init()))
      .pipe(concat("main.min.js"))
      .pipe(ngAnnotate())
      .pipe(uglify({ mangle: false }))
	  .pipe(gulpif(addSourcemaps, sourcemaps.write({includeContent: true})))
      .pipe(gulp.dest(paths.dist.scripts));
});
gulp.task('images', function() {
	return gulp.src(paths.src.images+'/**/*')
	.pipe(gulp.dest(paths.dist.images));
});
gulp.task('files', function() {
	return gulp.src(paths.src.files+'/**/*')
	.pipe(gulp.dest(paths.dist.files));
});


// I8,        8        ,8I                                88           
// `8b       d8b       d8'              ,d                88           
//  "8,     ,8"8,     ,8"               88                88           
//   Y8     8P Y8     8P  ,adPPYYba,  MM88MMM  ,adPPYba,  88,dPPYba,   
//   `8b   d8' `8b   d8'  ""     `Y8    88    a8"     ""  88P'    "8a  
//    `8a a8'   `8a a8'   ,adPPPPP88    88    8b          88       88  
//     `8a8'     `8a8'    88,    ,88    88,   "8a,   ,aa  88       88  
//      `8'       `8'     `"8bbdP"Y8    "Y888  `"Ybbd8"'  88       88  

gulp.task('watch', function() {
	// HTML
    gulp.watch(paths.src.src+'/**/*.html', gulp.series('html', 'reload')).on('error', function(){});
    
	// Styles
    gulp.watch(paths.src.styles+'/**/*.scss', gulp.series('sass', 'reload')).on('error', function(){});
    
    gulp.watch(paths.src.styles+'/**/*.min.css', gulp.series('css:min', 'reload')).on('error', function(){});
    
	// Scripts
    gulp.watch([paths.src.src+'/**/*.js', paths.src.scripts+'/**/*.min.js'], gulp.series('scripts:concat', 'reload')).on('error', function(){});
    gulp.watch(dependencyScripts, gulp.series('scripts:concat:dependencies', 'reload')).on('error', function(){});
    
	// Images
	gulp.watch(paths.src.images+'/**/*', gulp.series('images', 'reload')).on('error', function(){});
	
    // Files
	gulp.watch(paths.src.files+'/**/*', gulp.series('files', 'reload')).on('error', function(){});
});
gulp.task('default', gulp.series('watch'));



// 88888888ba                88  88           88  
// 88      "8b               ""  88           88  
// 88      ,8P                   88           88  
// 88aaaaaa8P'  88       88  88  88   ,adPPYb,88  
// 88""""""8b,  88       88  88  88  a8"    `Y88  
// 88      `8b  88       88  88  88  8b       88  
// 88      a8P  "8a,   ,a88  88  88  "8a,   ,d88  
// 88888888P"    `"YbbdP'Y8  88  88   `"8bbdP"Y8  

gulp.task('build',gulp.series('clean', gulp.parallel('html', 'sass', 'css:min', 'scripts:concat', 'scripts:concat:dependencies', 'images', 'files')));
