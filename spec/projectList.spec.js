describe("ProjectListController", function() {
    beforeEach(module('app'));
    
    var $controller;
    
    beforeEach(inject(function(_$controller_){
        $controller = _$controller_;
    }));
    
    describe('Sorting default', function() {
        it('sorts by amount raised by default', function() {
            var $scope = {};
            var controller = $controller('projectListController', { $scope: $scope });
            expect($scope.projectSort).toEqual('raised');
        });
    });
});