app.factory('projectListFactory', function($http) {
    var factory = {};
    
    factory.getProjects = function() {
        var request = {
            method: 'GET',
            url: '/get-projects'
        };
        
        return $http(request);
    }
    
    return factory;
});