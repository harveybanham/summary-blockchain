app.controller("projectListController", ['$scope', 'projectListFactory', function($scope, projectListFactory) {
    //List of funding types for filtering
    $scope.fundingTypes = [
        {
            "id" : "",
            "name" : "All funding types"
        },
        {
            "id" : "E",
            "name" : "Equity based"
        },
        {
            "id" : "R",
            "name" : "Reward based"
        }
    ];
    
    $scope.projectSort = "raised";
    $scope.projectFilter = {};
    $scope.projectFilter.funding_type = "";
    
    projectListFactory.getProjects().then(function successCallback(response) {
        //Success
        $scope.blockchain = response.data;
        
        angular.forEach($scope.blockchain, function(value) {
            //If date is after now, label as finished
            value.finished = "Finished";
            
             if(moment(value.end_time).isAfter(moment())) {
                 //If not finished, output time left
                 value.finished += " " + moment([value.end_time]).fromNow();
             }
             
             //Format end_time using moment
             value.end_time = moment(value.end_time).format();
             
             //Calculate percentage of goal reached
             value.goalPercent = (value.raised / value.goal) * 100;
             //Max percent to 100%, for md-progress-linear
             if(value.goalPercent > 100) {
                 value.goalPercent = 100;
             }
        });
        
    }, function errorCallback(response) {
        console.log("Error: " + response);
    });
}]);