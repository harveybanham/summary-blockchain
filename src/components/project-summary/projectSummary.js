app.directive("projectSummary", function() {
    return {
        templateUrl: '/components/project-summary/projectSummary.html',
        controller: function($scope) {
            $scope.getFundingTypeIcon = function(fundingType) {
                switch (fundingType) {
                    case "R":
                        return "reward";
                        break;
                    case "E":
                        return "equity";
                        break;
                    default:
                        return "unknown";
                }
            }
        },
        scope: {project: '='}
    };
});
