var fs = require('fs');
var https = require('https');
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
var cors = require('cors');
var fs = require('fs');

var blockchainProjectsFile = "blockchain-projects.json";

app.use(cors());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static('build'));

//For Heroku
var server_port = process.env.PORT || 3000;

app.listen(server_port, function() {
    console.log('listening on ' + server_port);
});


app.get('/', function (req, res) {
    res.sendFile(__dirname + '/build/index.html');
});

app.get('/get-projects', function (req, res) {
    res.sendFile(__dirname + "/" + blockchainProjectsFile);
});
